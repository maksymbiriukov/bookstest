// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "Navigation",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "Navigation",
            targets: ["Navigation"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "Navigation",
            dependencies: []),
        .testTarget(
            name: "NavigationTests",
            dependencies: ["Navigation"]),
    ],
    swiftLanguageVersions: [.v5]
)
