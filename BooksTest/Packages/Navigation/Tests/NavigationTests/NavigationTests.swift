import XCTest
@testable import Navigation

class FirstRouterMock: CommonRouter {}
class SecondRouterMock: CommonRouter {}
class ThirdRouterMock: CommonRouter {}

class CommonRouterScreensProviderMock: CommonRouterScreensProvider {
    func navigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func primaryNavigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func secondaryNavigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func emptyViewController() -> UIViewController {
        UIViewController()
    }
}

final class NavigationTests: XCTestCase {
    func testExample() throws {
        let mockProvider = CommonRouterScreensProviderMock()
        let firstRouter = FirstRouterMock(withScreensProvider: mockProvider)
        let secondRouter = SecondRouterMock(withScreensProvider: mockProvider, parentRouter: firstRouter, navigationController: UINavigationController())
        let thirdRouter = ThirdRouterMock(withScreensProvider: mockProvider, parentRouter: secondRouter, splitViewController: UISplitViewController())
        
        XCTAssertNoThrow(firstRouter)
        XCTAssertNoThrow(secondRouter)
        XCTAssertNoThrow(thirdRouter)
        
        XCTAssertNil(secondRouter.childRouter)
        XCTAssertNotNil(secondRouter.parentRouter)

        secondRouter.childRouter = thirdRouter

        XCTAssertNotNil(secondRouter.childRouter)

        XCTAssertTrue(secondRouter.parentRouter is FirstRouterMock)
        XCTAssertTrue(secondRouter.childRouter is ThirdRouterMock)

    }
}
