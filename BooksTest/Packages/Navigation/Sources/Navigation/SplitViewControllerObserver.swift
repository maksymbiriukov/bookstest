import UIKit

// MARK: - Protocol Definition

public protocol SplitViewControllerObservable: AnyObject {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool
    func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController?
}

// MARK: - Class Definition

public class SplitViewControllerObserver: NSObject {
    
    // MARK: Private Properties
    
    private weak var observer: SplitViewControllerObservable?
    
    // MARK: Init Methods & Superclass Overriders
    
    required public init(withObserver observer: SplitViewControllerObservable) {
        super.init()
        
        self.observer = observer
    }
}

// MARK: - UISplitViewControllerDelegate

extension SplitViewControllerObserver: UISplitViewControllerDelegate {
    public func splitViewController(_ splitViewController: UISplitViewController, show vc: UIViewController, sender: Any?) -> Bool {
        return true
    }
    
    public func splitViewController(_ splitViewController: UISplitViewController, showDetail vc: UIViewController, sender: Any?) -> Bool {
        return true
    }
    
    public func primaryViewController(forCollapsing splitViewController: UISplitViewController) -> UIViewController? {
        return splitViewController.viewControllers.first
    }
    
    public func primaryViewController(forExpanding splitViewController: UISplitViewController) -> UIViewController? {
        return splitViewController.viewControllers.first
    }
    
    public func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return (self.observer?.splitViewController(splitViewController, collapseSecondary: secondaryViewController, onto: primaryViewController) ?? false)
    }
    
    public func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {
        return self.observer?.splitViewController(splitViewController, separateSecondaryFrom: primaryViewController)
    }
}
