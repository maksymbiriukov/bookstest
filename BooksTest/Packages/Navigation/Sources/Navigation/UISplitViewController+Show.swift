//
//  File.swift
//  
//
//  Created by Maksym Biriukov on 18.03.2022.
//

import UIKit

public extension UISplitViewController {
    
    func show<T>(viewController: UIViewController, reset: Bool, emptyViewControllerClass: T.Type) {
        if self.isCollapsed {
            let navigationController = self.viewControllers.first as? UINavigationController
            navigationController?.pushViewController(viewController, animated: true)
        } else if let navigationController = self.viewControllers.last as? UINavigationController {
            if !reset && navigationController.viewControllers.filter({ !($0 is T) }).count > 0 {
                navigationController.pushViewController(viewController, animated: true)
            } else {
                let transition = CATransition()
                transition.duration = 0.25
                transition.type = CATransitionType.fade
                transition.subtype = nil
                
                navigationController.view.layer.add(transition, forKey: self.convertFromCATransitionType(CATransitionType.fade))
                navigationController.viewControllers = [viewController]
            }
        }
    }
    
    func reset(viewController: UIViewController, animated: Bool) {
        if let navigationController = self.viewControllers.last as? UINavigationController, self.viewControllers.count > 1 {
            if animated {
                let transition = CATransition()
                transition.duration = 0.25
                transition.type = CATransitionType.fade
                transition.subtype = nil
                
                navigationController.view.layer.add(transition, forKey: self.convertFromCATransitionType(CATransitionType.fade))
            }
            navigationController.viewControllers = [viewController]
        }
        
        if let navigationController = self.viewControllers.first as? UINavigationController {
            navigationController.popToRootViewController(animated: animated)
        }
    }
}

private extension UISplitViewController {
    
    func convertFromCATransitionType(_ input: CATransitionType) -> String {
        return input.rawValue
    }
}
