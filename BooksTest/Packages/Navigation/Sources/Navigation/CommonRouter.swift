import UIKit

// MARK: - Protocol Definition

public protocol CommonRouterScreensProvider: AnyObject {
    func navigationController() -> UINavigationController
    func primaryNavigationController() -> UINavigationController
    func secondaryNavigationController() -> UINavigationController
    func emptyViewController() -> UIViewController
}

// MARK: - Class Definition

open class CommonRouter {
    
    // MARK: Public Properties
    
    public var childRouter: CommonRouter?
    
    public private(set) weak var parentRouter: CommonRouter?
    public private(set) weak var screensProvider: CommonRouterScreensProvider?
    
    public private(set) var navigationController: UINavigationController!
    public private(set) var splitViewController: UISplitViewController?
    
    public private(set) lazy var splitViewControllerObserver = SplitViewControllerObserver(withObserver: self)

    // MARK: Init Methods & Superclass Overriders
    
    required public init(withScreensProvider provider: CommonRouterScreensProvider) {
        self.screensProvider = provider
        self.navigationController = provider.navigationController()
    }
    
    required public init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, navigationController: UINavigationController) {
        self.screensProvider = provider
        self.parentRouter = parentRouter
        self.navigationController = navigationController
    }
    
    required public init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, splitViewController: UISplitViewController) {
        self.screensProvider = provider
        self.parentRouter = parentRouter
        self.navigationController = provider.primaryNavigationController()
        self.splitViewController = splitViewController

        let emptyViewController = provider.emptyViewController()
        let secondaryNavigationController = provider.secondaryNavigationController()
        secondaryNavigationController.setNavigationBarHidden(true, animated: false)
        secondaryNavigationController.viewControllers = [emptyViewController]
        
        splitViewController.viewControllers = [self.navigationController, secondaryNavigationController]
        splitViewController.preferredDisplayMode = .allVisible
        splitViewController.delegate = self.splitViewControllerObserver
    }
}

extension CommonRouter: SplitViewControllerObservable {
    
    public func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        guard let provider = self.screensProvider, let secondaryNavigationController = secondaryViewController as? UINavigationController, let primaryNavigationController = primaryViewController as? UINavigationController else {
            return false
        }
        
        let emptyViewController = provider.emptyViewController()
        let additionalViewControllers = secondaryNavigationController.viewControllers.filter({ type(of: $0) != type(of: emptyViewController) })
        let currentViewControllers = primaryNavigationController.viewControllers
        let viewControllers = (currentViewControllers + additionalViewControllers)
        primaryNavigationController.viewControllers = viewControllers
        
        if splitViewController.isViewLoaded && splitViewController.view.window != nil {
            splitViewController.tabBarController?.tabBar.isHidden = (viewControllers.count > 1)
        }
        
        return true
    }
    
    public func splitViewController(_ splitViewController: UISplitViewController, separateSecondaryFrom primaryViewController: UIViewController) -> UIViewController? {
        guard let provider = self.screensProvider, let primaryNavigationController = primaryViewController as? UINavigationController else {
            return nil
        }
        
        let secondaryNavigationController = provider.secondaryNavigationController()
        let emptyViewController = provider.emptyViewController()
        
        if primaryNavigationController.viewControllers.count > 1 {
            secondaryNavigationController.viewControllers = Array(primaryNavigationController.viewControllers.suffix(from: 1))
            primaryNavigationController.viewControllers = Array(primaryNavigationController.viewControllers.prefix(1))
        } else {
            secondaryNavigationController.viewControllers = [emptyViewController]
        }
        
        if splitViewController.isViewLoaded && splitViewController.view.window != nil {
            splitViewController.tabBarController?.tabBar.isHidden = false
        }
        
        return secondaryNavigationController
    }
}
