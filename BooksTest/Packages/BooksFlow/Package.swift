// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BooksFlow",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "BooksFlow",
            targets: ["BooksFlow"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "BooksFlow",
            dependencies: []),
        .testTarget(
            name: "BooksFlowTests",
            dependencies: ["BooksFlow"]),
    ],
    swiftLanguageVersions: [.v5]

)
