import struct Foundation.URL

public struct BookDetailedItemViewModel {
    let title: String?
    let author: String?
    let description: String?
    
    let smallAssetURL: URL?
    let largeAssetURL: URL?
}
