import UIKit

import Extensions

class BookDetailedView: UIView {
        
    var viewStateViewModel: BookDetailedItemViewModel? {
        didSet {
            nameLabel.text = viewStateViewModel?.title
            genreLabel.text = viewStateViewModel?.author
            descriptionLabel.text = viewStateViewModel?.description
            
            if let url = viewStateViewModel?.smallAssetURL {
                posterBlurredImageView.loadImageWithUrl(url, contentMode: .scaleAspectFill, shouldBlur: true)
            }
            
            if let url = viewStateViewModel?.largeAssetURL {
                posterImageView.loadImageWithUrl(url, contentMode: .scaleAspectFit)
            }
        }
    }
    
    // MARK: - Outlets

    private let posterBlurredImageView: CachedImage = {
        let posterImage = CachedImage()
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.backgroundColor = .applicationContentColor
        posterImage.tintColor = .applicationFillColor
        posterImage.contentMode = .scaleAspectFit
        posterImage.layer.masksToBounds = true
        return posterImage
    }()
    
    private let posterContainerView: UIView = {
        let posterContainer = UIView()
        posterContainer.translatesAutoresizingMaskIntoConstraints = false
        posterContainer.clipsToBounds = false
        return posterContainer
    }()
    
    private let posterImageView: CachedImage = {
        let posterImage = CachedImage()
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        posterImage.backgroundColor = .applicationContentColor
        posterImage.tintColor = .applicationFillColor
        posterImage.contentMode = .center
        posterImage.clipsToBounds = true
        posterImage.isUserInteractionEnabled = true
        return posterImage
    }()
    
    private let nameLabel: UILabel = {
        let name: UILabel = UILabel()
        name.lineBreakMode = .byWordWrapping
        name.numberOfLines = 2
        name.textAlignment = .center
        name.adjustsFontForContentSizeCategory = true
        name.font = UIFont.preferredFont(forTextStyle: .largeTitle)
        return name
    }()
    
    private let descriptionLabel: UILabel = {
        let description: UILabel = UILabel()
        description.lineBreakMode = .byWordWrapping
        description.numberOfLines = 0
        description.textAlignment = .left
        description.adjustsFontForContentSizeCategory = true
        description.font = UIFont.preferredFont(forTextStyle: .body)
        return description
    }()

    private let genreLabel: UILabel = {
        let genre: UILabel = UILabel()
        genre.lineBreakMode = .byWordWrapping
        genre.textAlignment = .left
        genre.adjustsFontForContentSizeCategory = true
        genre.font = UIFont.preferredFont(forTextStyle: .title3)
        return genre
    }()
    
    private let scrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.isScrollEnabled = true
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    private let generalStackView: UIStackView = {
        let generalStack = UIStackView()
        generalStack.translatesAutoresizingMaskIntoConstraints = false
        generalStack.axis = .vertical
        generalStack.spacing = 8.0
        return generalStack
    }()
    
    private let generalContainerView: UIView = {
        let generalContainer = UIView()
        generalContainer.backgroundColor = UIColor.systemBackground
        generalContainer.translatesAutoresizingMaskIntoConstraints = false
        return generalContainer
    }()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    // MARK: - Private
    
    private func setupViews() {
        backgroundColor = .systemBackground
        addSubview(posterBlurredImageView)
        addSubview(scrollView)
        
        scrollView.addSubview(generalContainerView)
        scrollView.addSubview(posterContainerView)
        
        generalContainerView.addSubview(generalStackView)
        
        generalStackView.addArrangedSubview(nameLabel)
        generalStackView.addArrangedSubview(genreLabel)
        generalStackView.addArrangedSubview(descriptionLabel)
        
        posterContainerView.addSubview(posterImageView)
        
        // scroll
        scrollView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        // container
        generalContainerView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        
        generalStackView.topAnchor.constraint(equalTo: generalContainerView.topAnchor, constant: 20.0).isActive = true
        generalStackView.bottomAnchor.constraint(equalTo: generalContainerView.bottomAnchor, constant: -8.0).isActive = true
        generalStackView.leadingAnchor.constraint(equalTo: generalContainerView.leadingAnchor, constant: 8.0).isActive = true
        generalStackView.trailingAnchor.constraint(equalTo: generalContainerView.trailingAnchor, constant: -8.0).isActive = true
        
        // images
        posterContainerView.leadingAnchor.constraint(equalTo: posterImageView.leadingAnchor).isActive = true
        posterContainerView.trailingAnchor.constraint(equalTo: posterImageView.trailingAnchor).isActive = true
        posterContainerView.topAnchor.constraint(equalTo: posterImageView.topAnchor).isActive = true
        posterContainerView.bottomAnchor.constraint(equalTo: posterImageView.bottomAnchor).isActive = true
        
        posterImageView.centerXAnchor.constraint(equalTo: posterBlurredImageView.centerXAnchor).isActive = true
        posterImageView.bottomAnchor.constraint(equalTo: generalContainerView.topAnchor, constant: 20.0).isActive = true
        posterImageView.widthAnchor.constraint(equalTo: posterBlurredImageView.widthAnchor, multiplier: 1/3).isActive = true
        posterImageView.heightAnchor.constraint(equalTo: posterImageView.widthAnchor, multiplier: 3/2).isActive = true
        
        posterBlurredImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        posterBlurredImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        posterBlurredImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        posterBlurredImageView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        posterBlurredImageView.heightAnchor.constraint(equalTo: widthAnchor).isActive = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        posterContainerView.layer.shadowColor = UIColor.black.cgColor
        posterContainerView.layer.shadowOpacity = 0.4
        posterContainerView.layer.shadowOffset = .zero
        posterContainerView.layer.shadowRadius = 4
        posterContainerView.layer.shadowPath = UIBezierPath(roundedRect: posterContainerView.bounds, cornerRadius: 10).cgPath
        
        posterImageView.layer.cornerRadius = 10.0
        
        let path = UIBezierPath(roundedRect: generalContainerView.bounds,
                                byRoundingCorners: [.topLeft, .topRight],
                                cornerRadii: CGSize(width: 16.0, height: 16.0))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        generalContainerView.layer.mask = maskLayer
        
        let difference = posterBlurredImageView.frame.height - posterImageView.frame.height
        scrollView.contentInset = UIEdgeInsets(top: difference, left: 0.0, bottom: difference, right: 0.0)
        scrollView.contentSize = generalContainerView.bounds.size
    }
}
