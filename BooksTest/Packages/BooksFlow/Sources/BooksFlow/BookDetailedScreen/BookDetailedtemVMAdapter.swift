import DataLayer

struct BookDetailedtemVMAdapter {
    
    let book: BookModel
    
    init(book: BookModel) {
        self.book = book
    }
    
    func vm() -> BookDetailedItemViewModel {
        let model = BookDetailedItemViewModel(title: book.volumeInfo?.title,
                                              author: book.volumeInfo?.authors?.joined(separator: ", "),
                                              description: book.volumeInfo?.description,
                                              smallAssetURL: book.volumeInfo?.imageLinks?.smallThumbnail,
                                              largeAssetURL: book.volumeInfo?.imageLinks?.thumbnail)
        return model
    }
}
