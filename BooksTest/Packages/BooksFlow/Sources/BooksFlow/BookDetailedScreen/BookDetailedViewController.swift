import UIKit

class BookDetailedViewController: UIViewController {
    
    // MARK: - Outlets & Variables
    
    private var detailedView: BookDetailedView {
        return view as! BookDetailedView
    }

    private let item: BookDetailedItemViewModel

    // MARK: - Initialization
    
    required init(item: BookDetailedItemViewModel) {
        self.item = item
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = BookDetailedView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        detailedView.viewStateViewModel = item
    }
}
