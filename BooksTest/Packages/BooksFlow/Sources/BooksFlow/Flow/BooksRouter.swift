import UIKit

import Navigation
import Networking
import struct DataLayer.BookModel

public class BooksRouter: CommonRouter {
                        
    // MARK: Init Methods & Superclass Overriders
    
    public required init(withParentRouter parentRouter: CommonRouter, screensAssembly: CommonRouterScreensProvider, navigationController: UINavigationController) {
        super.init(withScreensProvider: screensAssembly, parentRouter: parentRouter, navigationController: navigationController)
    }
    
    public required init(withScreensProvider provider: CommonRouterScreensProvider) {
        fatalError("init(withScreensProvider:) has not been implemented")
    }
    
    public required init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, navigationController: UINavigationController) {
        fatalError("init(withScreensProvider:parentRouter:navigationController:) has not been implemented")
    }
    
    public required init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, splitViewController: UISplitViewController) {
        super.init(withScreensProvider: provider, parentRouter: parentRouter, splitViewController: splitViewController)
    }
}

// MARK: RootRoutesProviderable

extension BooksRouter: BooksRoutesProviderable {
    
    public func showBooksListScreen() {
        let environment = GoogleBooksEnvironment()
        let fetcher = BooksListFetcherImpl(environment: environment)
        let viewModel = BookListViewModel(router: self, networkService: fetcher)
        let viewController = BooksListViewController(viewModel: viewModel)
        let navigationController = (self.splitViewController?.viewControllers.first as? UINavigationController)
        navigationController?.setViewControllers([viewController], animated: false)
    }
    
    public func showBookDetailedScreen(with item: BookDetailedItemViewModel) {
        let viewController = BookDetailedViewController(item: item)
        self.splitViewController?.show(viewController: viewController, reset: true, emptyViewControllerClass: EmptyBookView.self)
    }
}

// TODO: Move and inject from services class
class GoogleBooksEnvironment: NetworkEnvironment {
    var scheme: String = "https"
    var host: String = "www.googleapis.com"
    var path: String = "/books/v1/volumes"
}
