public protocol BooksRoutesProviderable: AnyObject {
    func showBooksListScreen()
    func showBookDetailedScreen(with item: BookDetailedItemViewModel)
}
