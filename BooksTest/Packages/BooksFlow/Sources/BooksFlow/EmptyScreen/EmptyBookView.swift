import UIKit

public class EmptyBookView: UIViewController {
    
    let assetImageView: UIImageView = {
        let asset = UIImageView()
        asset.contentMode = .center
        asset.clipsToBounds = true
        asset.layer.cornerRadius = 8.0
        
        let largeConfiguration = UIImage.SymbolConfiguration(scale: .large)
        asset.image = UIImage(systemName: "book", withConfiguration: largeConfiguration)
        
        asset.translatesAutoresizingMaskIntoConstraints = false
        asset.tintColor = .applicationTintColor
        return asset
    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .applicationContentColor
        
        self.view.addSubview(assetImageView)
        
        assetImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        assetImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
