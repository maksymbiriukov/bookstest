import UIKit

struct BookListDatasourceItem {
    let thumbnailUrl: URL?
    let title: String?
    let author: String?
}

class BookListDatasource: NSObject {
    
    var data: [BookListDatasourceItem] = []
}

extension BookListDatasource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BookListTableViewCell", for: indexPath) as! BookListTableViewCell
        cell.cellViewModel = data[indexPath.row]
        return cell
    }
}
