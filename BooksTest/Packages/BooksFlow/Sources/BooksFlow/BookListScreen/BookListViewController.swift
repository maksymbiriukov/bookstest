import UIKit

class BooksListViewController: UIViewController {
    
    // MARK: - Outlets & Variables
    
    private var booksView: BookListView {
        return view as! BookListView
    }
    
    private var viewModel: BookListViewModel
    
    // MARK: - Initialization
    
    required init(viewModel: BookListViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Lifecycle
    
    override func loadView() {
        view = BookListView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Books List"
        self.navigationItem.largeTitleDisplayMode = .always
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        definesPresentationContext = true

        bindUI()
    }
    
    private func bindUI() {
        booksView.delegate = self
        
        viewModel.reloadData = { [weak self] datasource in
            DispatchQueue.main.async {
                self?.booksView.data = datasource
            }
        }
        
        viewModel.showError = { [weak self] in
            DispatchQueue.main.async {
                self?.showError()
            }
        }
        
        viewModel.updateLoadingStatus = { [weak self] in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async {
                if self.viewModel.isLoading {
                    self.booksView.showActivityIndicator()
                } else {
                    self.booksView.hideActivityIndicator()
                }
            }
        }
    }
    
    private func showError() {
        let alert = UIAlertController(title: "Error", message: viewModel.error?.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}

// MARK: - BooksListViewDelegate

extension BooksListViewController: BooksListViewDelegate {
    
    func bookSelected(at indexPath: IndexPath) {
        viewModel.shouldShowBook(at: indexPath)
    }
    
    func shouldSearch(term: String) {
        viewModel.searchTerm = term
    }
}
