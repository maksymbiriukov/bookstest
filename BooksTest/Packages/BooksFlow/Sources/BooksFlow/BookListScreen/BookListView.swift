import UIKit

import UIKit

protocol BooksListViewDelegate: AnyObject {
    func bookSelected(at indexPath: IndexPath)
    func shouldSearch(term: String)
}

class BookListView: UIView {
        
    // MARK: - Public Properties

    weak var delegate: BooksListViewDelegate? = nil
    
    var data: [BookListDatasourceItem] = [] {
        didSet {
            self.dataSource.data = data
            self.tableView.reloadData()
        }
    }

    // MARK: - Outlets
    
    lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .plain)
        
        tableView.backgroundColor = .applicationFillColor
        
        tableView.translatesAutoresizingMaskIntoConstraints = false

        tableView.delegate = self
        tableView.dataSource = dataSource
        
        tableView.register(BookListTableViewCell.self, forCellReuseIdentifier: "BookListTableViewCell")
        
        tableView.estimatedRowHeight = 60.0
        
        tableView.allowsSelection = true
        
        return tableView
    }()
    
    private lazy var searchController: UISearchController =  {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        return searchController
    }()

    private let activityIndicator: UIActivityIndicatorView = {
        let activity: UIActivityIndicatorView
        activity = UIActivityIndicatorView(style: .medium)
        activity.isHidden = true
        activity.translatesAutoresizingMaskIntoConstraints = false
        return activity
    }()
    
    // MARK: - Private Properties
    
    private var dataSource = BookListDatasource()
    
    // MARK: - Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func showActivityIndicator() {
        bringSubviewToFront(activityIndicator)
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
        tableView.isUserInteractionEnabled = false
    }
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        tableView.isUserInteractionEnabled = true
    }

    // MARK: - Private
    
    private func setupViews() {
        backgroundColor = .applicationFillColor

        addSubview(tableView)
        addSubview(activityIndicator)

        activityIndicator.centerXAnchor.constraint(equalTo: tableView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: tableView.centerYAnchor).isActive = true

        tableView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        
        tableView.tableHeaderView = searchController.searchBar
    }
}

// MARK: - UITableViewDelegate

extension BookListView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.bookSelected(at: indexPath)
    }
}

// MARK: - UISearchResultsUpdating

extension BookListView: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {}
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let term = searchBar.text, !term.isEmpty else {
            return
        }
        
        searchBar.resignFirstResponder()
        
        delegate?.shouldSearch(term: term)
    }
}
