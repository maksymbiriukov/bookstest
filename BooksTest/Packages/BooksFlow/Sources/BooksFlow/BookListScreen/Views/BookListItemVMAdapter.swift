import DataLayer

struct BookListItemVMAdapter {
    
    let response: BooksModel
    
    init(response: BooksModel) {
        self.response = response
    }
    
    func vm() -> [BookListDatasourceItem] {
        let items: [BookListDatasourceItem?] = (response.items ?? [])
            .compactMap{ $0.volumeInfo }
            .compactMap { BookListDatasourceItem(thumbnailUrl: $0.imageLinks?.smallThumbnail,
                                                 title: $0.title,
                                                 author: $0.authors?.joined(separator: ", "))}
        return items.compactMap{$0}
    }
}
