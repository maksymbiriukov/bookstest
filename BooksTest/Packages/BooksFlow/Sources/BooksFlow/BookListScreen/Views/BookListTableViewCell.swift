import UIKit

import Extensions

class BookListTableViewCell: UITableViewCell {
    
    // MARK: - Binding
    
    var cellViewModel: BookListDatasourceItem? {
        didSet {
            self.titleLabel.isHidden = cellViewModel?.title == nil
            self.authorLabel.isHidden = cellViewModel?.author == nil

            self.titleLabel.text = cellViewModel?.title
            self.authorLabel.text = cellViewModel?.author

            if let url = cellViewModel?.thumbnailUrl {
                assetImageView.loadImageWithUrl(url, contentMode: .scaleToFill)
            }
        }
    }
    
    // MARK: - Outlets
    
    let containerView: UIView = {
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        container.backgroundColor = .applicationContentColor
        return container
    }()
    
    let assetImageView: CachedImage = {
        let asset = CachedImage()
        asset.contentMode = .center
        asset.clipsToBounds = true
        asset.layer.cornerRadius = 8.0
        asset.image = UIImage(systemName: "book")
        asset.translatesAutoresizingMaskIntoConstraints = false
        asset.tintColor = .applicationTintColor
        return asset
    }()
    
    private let titleLabel: UILabel = {
        let label: UILabel = UILabel()
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 1
        label.textAlignment = .left
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.tintColor = .applicationFillColor
        return label
    }()
    
    private let authorLabel: UILabel = {
        let label: UILabel = UILabel()
        label.lineBreakMode = .byTruncatingTail
        label.numberOfLines = 2
        label.textAlignment = .left
        label.adjustsFontForContentSizeCategory = true
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.tintColor = .applicationFillColor
        return label
    }()
    
    // MARK: - Initialization
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        assetImageView.contentMode = .center
        assetImageView.image = UIImage(systemName: "book")
    }
    
    func setupViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(assetImageView)
        
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8.0
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        stack.addArrangedSubview(titleLabel)
        stack.addArrangedSubview(authorLabel)
        
        containerView.addSubview(stack)

        containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        containerView.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
        assetImageView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16.0).isActive = true
        assetImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8.0).isActive = true
        assetImageView.bottomAnchor.constraint(lessThanOrEqualTo: containerView.bottomAnchor, constant: -8.0).isActive = true
        assetImageView.heightAnchor.constraint(equalToConstant: 60.0).isActive = true
        assetImageView.widthAnchor.constraint(equalToConstant: 44.0).isActive = true
        
        stack.leadingAnchor.constraint(equalTo: assetImageView.trailingAnchor, constant: 8.0).isActive = true
        stack.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 8.0).isActive = true
        stack.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -8.0).isActive = true
        stack.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16.0).isActive = true
    }
}
