
import Foundation
import Combine

import Networking
import DataLayer

class BookListViewModel {
        
    init(router: BooksRoutesProviderable, networkService: BooksListFetcher) {
        self.router = router
        self.networkService = networkService
    }
    
    // MARK: - Outputs
    
    var reloadData: (([BookListDatasourceItem]) -> Void)?
    var showError: (() -> Void)?
    var updateLoadingStatus: (() -> Void)?
    
    var isLoading: Bool = false {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var error: Error? = .none {
        didSet {
            self.showError?()
        }
    }
    
    var itemsCount: Int {
        return datasource.count
    }
    
    func cellViewModel(for indexPath: IndexPath) -> BookListDatasourceItem {
        return datasource[indexPath.row]
    }
    
    // MARK: - Inputs
    
    var searchTerm: String = "" {
        didSet {
            fetchBooks()
        }
    }
    
    func shouldShowBook(at indexPath: IndexPath) {
        guard let model = apiModel?.items?[indexPath.row] else {
            return
        }
        router.showBookDetailedScreen(with: BookDetailedtemVMAdapter(book: model).vm())
    }
    
    // MARK: - Private
    
    private var datasource = [BookListDatasourceItem]() {
        didSet {
            self.reloadData?(datasource)
        }
    }
    private var apiModel: BooksModel?
        
    private unowned var router: BooksRoutesProviderable!
    private var networkService: BooksListFetcher!
        
    private func fetchBooks() {
        self.datasource = []
        
        isLoading = true
        
        let requestModel = BooksListRequstModel(query: searchTerm, startIndex: 0)
        
        networkService.getBooksList(input: requestModel) { [weak self] (result: Result<BooksModel, NetworkingError>) in
            guard let self = self else {
                return
            }

            self.isLoading = false

            switch result {
            case .success(let response):
                self.apiModel = response
                self.datasource = BookListItemVMAdapter(response: response).vm()
            case .failure(let error):
                self.error = error as Error
            }
        }
    }
}
