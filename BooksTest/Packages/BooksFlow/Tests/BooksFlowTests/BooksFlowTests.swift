import XCTest
@testable import BooksFlow

final class BooksFlowTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(BooksFlow().text, "Hello, World!")
    }
}
