import UIKit

public extension UIColor {
    
    static var applicationFillColor: UIColor {
        return UIColor(named: "applicationFillColor")!
    }
    
    static var applicationTintColor: UIColor {
        return UIColor(named: "applicationTintColor")!
    }
    
    static var applicationContentColor: UIColor {
        return UIColor(named: "applicationContentColor")!
    }
}
