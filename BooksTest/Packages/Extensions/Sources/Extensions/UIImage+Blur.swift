import UIKit
import Accelerate

extension UIImage {
    
    func blurred(for radius: CGFloat = 10.0) -> UIImage? {
        let boxSize = radius + 1
        
        guard let image = self.cgImage, let inProvider = image.dataProvider else {
            return nil
        }
        
        let height = vImagePixelCount(image.height)
        let width = vImagePixelCount(image.width)
        let rowBytes = image.bytesPerRow
        
        let inBitmapData = inProvider.data
        let inData = UnsafeMutableRawPointer(mutating: CFDataGetBytePtr(inBitmapData))
        var inBuffer = vImage_Buffer(data: inData, height: height, width: width, rowBytes: rowBytes)
        
        let outData = malloc(image.bytesPerRow * image.height)
        var outBuffer = vImage_Buffer(data: outData, height: height, width: width, rowBytes: rowBytes)
        
        let _ = vImageBoxConvolve_ARGB8888(&inBuffer, &outBuffer, nil, 0, 0, UInt32(boxSize), UInt32(boxSize), nil, vImage_Flags(kvImageEdgeExtend))
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        guard let context = CGContext(data: outBuffer.data,
                                      width: Int(outBuffer.width),
                                      height: Int(outBuffer.height),
                                      bitsPerComponent: 8,
                                      bytesPerRow: outBuffer.rowBytes,
                                      space: colorSpace,
                                      bitmapInfo: image.bitmapInfo.rawValue),
            let imageRef = context.makeImage() else {
                return nil
        }
        
        let bluredImage = UIImage(cgImage: imageRef)
        
        free(outData)
        
        return bluredImage
    }
}
