import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

public class CachedImage: UIImageView {

    var imageURL: URL?

    let activityIndicator = UIActivityIndicatorView()

    public func loadImageWithUrl(_ url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit, shouldBlur: Bool = false) {

        activityIndicator.color = .darkGray

        addSubview(activityIndicator)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true

        imageURL = url

        image = nil
        activityIndicator.startAnimating()

        if let imageFromCache = imageCache.object(forKey: url as AnyObject) as? UIImage {
            self.image = imageFromCache
            activityIndicator.stopAnimating()
            return
        }

        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            guard let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                  let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                  let data = data, error == nil,
                  var image = UIImage(data: data) else {
                      self.activityIndicator.stopAnimating()
                      return
                  }
            
            DispatchQueue.main.async(execute: {
                
                if let imageToCache = UIImage(data: data) {

                    if self.imageURL == url {
                        if shouldBlur {
                            image = image.blurred() ?? image
                        }
                        self.image = image
                        self.contentMode = mode
                    }

                    imageCache.setObject(imageToCache, forKey: url as AnyObject)
                }
                self.activityIndicator.stopAnimating()
            })
        }).resume()
    }
}
