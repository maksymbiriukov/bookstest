import XCTest
@testable import DataLayer

final class DataLayerTests: XCTestCase {
    
    private enum ParsingError: Error, LocalizedError {
        case badFile
        case parsingError(Error)
        case unknown
        
        var errorDescription: String? {
            switch self {
            case .badFile:
                return "Bad file"
            case .parsingError(let error):
                return "Parsing error: \(error.localizedDescription)"
            case .unknown:
                return "Unknown error"
            }
        }
    }
        
    func testBooksModel() throws {
        let fileModel: BooksModel?
        
        do {
            fileModel = try parse(from: "volumes")
        } catch let error {
            XCTFail("\(error.localizedDescription)")
            return
        }
        
        guard let fileModel = fileModel else {
            XCTFail("\(ParsingError.unknown.localizedDescription)")
            return
        }
        
        XCTAssertNotNil(fileModel.kind)
        XCTAssertNotNil(fileModel.totalItems)
        XCTAssertNotNil(fileModel.items)
        
        XCTAssertFalse(fileModel.items.isEmpty)
        
        XCTAssertEqual(fileModel.kind, "books#volumes")
        XCTAssertEqual(fileModel.totalItems, 564)

        guard let firstBook = fileModel.items.first else {
            XCTFail("Empty books")
            return
        }
        XCTAssertNotNil(firstBook.kind)
        XCTAssertNotNil(firstBook.id)
        XCTAssertNotNil(firstBook.volumeInfo)
        
        XCTAssertEqual(firstBook.kind, "books#volume")
        XCTAssertEqual(firstBook.id, "u13hVoYVZa8C")
        
        guard let volumeInfo = firstBook.volumeInfo else {
            XCTFail("Empty volume info")
            return
        }
        XCTAssertEqual(volumeInfo.title, "Planning Extreme Programming")
        XCTAssertEqual(volumeInfo.authors, ["Kent Beck", "Mike Hendrickson", "Martin Fowler"])
        XCTAssertEqual(volumeInfo.publisher, "Addison-Wesley Professional")
        XCTAssertEqual(volumeInfo.description, """
                    A guide to XP leads the developer, project manager, \
                    and team leader through the software development planning process, \
                    offering real world examples and tips for reacting to changing environments quickly and efficiently.
                    """)
        
        guard let imageInfo = volumeInfo.imageLinks else {
            XCTFail("Empty image info")
            return
        }
        
        let smallURL = URL(string: "http://books.google.com/books/content?id=u13hVoYVZa8C&printsec=frontcover&img=1&zoom=5&edge=curl&source=gbs_api")
        let largeURL = URL(string: "http://books.google.com/books/content?id=u13hVoYVZa8C&printsec=frontcover&img=1&zoom=1&edge=curl&source=gbs_api")
        
        XCTAssertEqual(imageInfo.smallThumbnail, smallURL)
        XCTAssertEqual(imageInfo.thumbnail, largeURL)
    }
    
    func testBooksParsingBadFile() {
        XCTAssertThrowsError(try parse(from: "UNKNOWN_FILE"))
    }
}

private extension DataLayerTests {
    func parse(from file: String) throws -> BooksModel {
        guard let path = Bundle.module.path(forResource: file, ofType: "json") else {
            throw ParsingError.badFile
        }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            let model = try JSONDecoder().decode(BooksModel.self, from: data)
            return model
        } catch let error {
            throw ParsingError.parsingError(error)
        }
    }
}
