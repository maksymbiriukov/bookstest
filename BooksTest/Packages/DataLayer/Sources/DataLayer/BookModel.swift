public struct BookModel: Decodable {

    public var kind: String?
    
    public var id: String?

    public var volumeInfo: VolumeInfoModel?
}
