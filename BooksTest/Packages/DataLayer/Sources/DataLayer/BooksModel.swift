public struct BooksModel: Decodable {

    public var kind: String?
    
    public var totalItems: Int?

    public var items: [BookModel]? = []
}
