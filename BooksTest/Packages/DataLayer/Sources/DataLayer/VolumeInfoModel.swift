public struct VolumeInfoModel: Decodable {

    public var title: String?
    
    public var authors: [String]? = []
    
    public var publisher: String?
    
    public var description: String?

    public var imageLinks: ImageLinksModel?
}
