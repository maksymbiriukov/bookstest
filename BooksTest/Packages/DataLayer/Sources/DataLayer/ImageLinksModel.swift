import Foundation

public struct ImageLinksModel: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case smallThumbnail
        case thumbnail
    }
    
    public var smallThumbnail: URL?
    
    public var thumbnail: URL?
}

extension ImageLinksModel {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.smallThumbnail = try container.decode(URL.self, forKey: .smallThumbnail)
        self.thumbnail = try container.decode(URL.self, forKey: .thumbnail)
    }
}
