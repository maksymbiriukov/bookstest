// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "DataLayer",
    platforms: [.iOS(.v13)],
    products: [
        .library(
            name: "DataLayer",
            targets: ["DataLayer"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "DataLayer",
            dependencies: []),
        .testTarget(
            name: "DataLayerTests",
            dependencies: ["DataLayer"],
            resources: [.process("Resources")]),
        
    ],
    swiftLanguageVersions: [.v5]
)
