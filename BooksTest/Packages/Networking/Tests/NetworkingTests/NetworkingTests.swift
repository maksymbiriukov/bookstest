import XCTest
@testable import Networking

struct MockNetworkEnvironment: NetworkEnvironment {
    var scheme: String
    var host: String
    var path: String
}

final class NetworkingTests: XCTestCase {
    
    private let scheme = "https"
    private let host = "test.host.com"
    private let path = "/api/v1"
    
    private lazy var environment: NetworkEnvironment = MockNetworkEnvironment(scheme: scheme, host: host, path: path)

    func testEnvironment() throws {
        XCTAssertEqual(scheme, environment.scheme)
        XCTAssertEqual(host, environment.host)
        XCTAssertEqual(path, environment.path)
    }
    
    func testRequestBuilder() throws {
        let builder = RequestBuilder(environment: environment)
        
        XCTAssertTrue(builder.queryItems.isEmpty)
        
        let request = builder.asURLRequest()
        XCTAssertEqual(request.url, URL(string: "https://test.host.com/api/v1"))
        
        builder.queryItems.append(URLQueryItem(name: "q", value: "book"))
        
        let requestWithURLParams = builder.asURLRequest()
        XCTAssertEqual(requestWithURLParams.url, URL(string: "https://test.host.com/api/v1?q=book"))
    }
}
