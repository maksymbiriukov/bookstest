import Foundation

class RequestBuilder {
    
    required init(environment: NetworkEnvironment) {
        self.environment = environment
    }
    
    var environment: NetworkEnvironment
    var queryItems: [URLQueryItem] = []
    
    func asURLRequest() -> URLRequest {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = environment.scheme
        urlComponents.host = environment.host
        urlComponents.path = environment.path
        if !queryItems.isEmpty {
            urlComponents.queryItems = queryItems
        }
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        return request
    }
}
