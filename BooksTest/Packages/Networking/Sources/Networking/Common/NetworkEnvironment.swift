public protocol NetworkEnvironment {
    var scheme: String { get }
    var host : String { get }
    
    var path: String { get }
}
