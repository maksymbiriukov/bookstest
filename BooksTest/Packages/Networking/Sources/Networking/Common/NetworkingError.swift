import Foundation

public indirect enum NetworkingError: Error {
    
    case unknown

    case server
    case client
    case connectionOffline
    case requestTimeout
    case parsingError
}
