import Foundation

public protocol BooksListFetcher: AnyObject {
    func getBooksList<T: Decodable>(input: BooksListRequstModel,
                                    completion: @escaping (Result<T, NetworkingError>) -> Void)
}

public class BooksListFetcherImpl: BooksListFetcher {
    
    var environment: NetworkEnvironment
    
    private let decoder = JSONDecoder()
    
    private var dataTask: URLSessionDataTask?
    
    public init(environment: NetworkEnvironment) {
        self.environment = environment
    }
    
    // TODO: - override dataTaskPublisher to mocking result
    
    public func getBooksList<T: Decodable>(input: BooksListRequstModel,
                                    completion: @escaping (Result<T, NetworkingError>) -> Void) {
        
        let requestBuilder = RequestBuilder(environment: environment)
        
        var queryItems = [URLQueryItem]()
        queryItems.append(URLQueryItem(name: "q", value: input.query))
        queryItems.append(URLQueryItem(name: "maxResults", value: String(input.maxResults)))

        if input.startIndex > 0 {
            queryItems.append(URLQueryItem(name: "startIndex", value: String(input.startIndex)))
        }
                    
        requestBuilder.queryItems = queryItems
                
        let request = requestBuilder.asURLRequest()
        
        dataTask = URLSession.shared.dataTask(with: request, completionHandler: { data, response, error in
            guard let httpResponse = response as? HTTPURLResponse,
                  200..<300 ~= httpResponse.statusCode else {
                      switch (response as! HTTPURLResponse).statusCode {
                      case (400..<500):
                          completion(.failure(NetworkingError.client))
                      case (500..<600):
                          completion(.failure(NetworkingError.server))
                      default:
                          completion(.failure(NetworkingError.unknown))
                      }
                      return
                  }
            do {
                if let data = data {
                    let value = try self.decoder.decode(T.self, from: data)
                    completion(.success(value))
                } else {
                    completion(.failure(NetworkingError.parsingError))
                }
            } catch let error {
                print(error)
                completion(.failure(NetworkingError.parsingError))
            }
        })
        dataTask?.resume()
    }
}

