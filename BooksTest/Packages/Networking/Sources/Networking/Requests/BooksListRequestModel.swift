public struct BooksListRequstModel {
    public let query: String
    
    public let startIndex: Int

    /// In accordance with https://developers.google.com/books/docs/v1/using
    public let maxResults: Int = 20
    
    public init(query: String, startIndex: Int) {
        self.query = query
        self.startIndex = startIndex
    }
}
