//
//  ApplicationRouter.swift
//  BooksTest
//
//  Created by Maksym Biriukov on 18.03.2022.
//

import UIKit
import Navigation
import BooksFlow

protocol InitialRoutesProviderable {
    func showInitialScreen(withWindow window: UIWindow)
}

class ApplicationRouter: CommonRouter {
    
    // MARK: Private Properties
    
    private var window: UIWindow?
    private var provider: CommonRouterScreensProvider
    
    // MARK: Init Methods & Superclass Overriders
    
    required init() {
        self.provider = ScreenProvider()
        super.init(withScreensProvider: provider)
    }
    
    required init(withScreensProvider provider: CommonRouterScreensProvider) {
        fatalError("init(withScreensProvider:) has not been implemented")
    }
    
    required init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, navigationController: UINavigationController) {
        fatalError("init(withScreensProvider:parentRouter:navigationController:) has not been implemented")
    }
    
    required init(withScreensProvider provider: CommonRouterScreensProvider, parentRouter: CommonRouter, splitViewController: UISplitViewController) {
        fatalError("init(withScreensProvider:parentRouter:splitViewController:) has not been implemented")
    }
}

// MARK: InitialRoutesProviderable

extension ApplicationRouter: InitialRoutesProviderable {
    func showInitialScreen(withWindow window: UIWindow) {
        self.window = window
        
        let splitViewController = UISplitViewController()
                
        window.rootViewController = splitViewController
        
        self.showRootScreen(with: splitViewController)
        
        window.makeKeyAndVisible()
    }
}

// MARK: Root

extension ApplicationRouter {
    func showRootScreen(with splitViewController: UISplitViewController) {
        let router = BooksRouter(withScreensProvider: self.provider, parentRouter: self, splitViewController: splitViewController)
        
        self.childRouter = router
        
        router.showBooksListScreen()
    }
}

// MARK: Support Methods

private extension ApplicationRouter {
    
    func applicationWindow() -> UIWindow {
        guard let window = self.window else {
            fatalError("Window shouldn't be nil")
        }
        
        return window
    }
}

// MARK: - ScreenProvider

class ScreenProvider: CommonRouterScreensProvider {
    func navigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func primaryNavigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func secondaryNavigationController() -> UINavigationController {
        UINavigationController()
    }
    
    func emptyViewController() -> UIViewController {
        EmptyBookView()
    }
}
