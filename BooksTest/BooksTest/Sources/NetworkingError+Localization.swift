//
//  NetworkingError+Localization.swift
//  BooksTest
//
//  Created by Maksym Biriukov on 18.03.2022.
//

import Foundation
import Networking

extension NetworkingError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .unknown:
            return "Unknown error"
        case .server:
            return "Server error"
        case .client:
            return "Client error"
        case .connectionOffline:
            return "No connection"
        case .requestTimeout:
            return "Request timeout"
        case .parsingError:
            return "Parsing error"
        }
    }
}
